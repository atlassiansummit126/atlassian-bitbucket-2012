How to add sudokus to a web page


The Python Sudoku web has a script to get images of sudokus in PNG format. That script can be used to add sudokus to a web page.
Daily 4 sudokus are added, 1 for each of this handicaps: 0, 5, 10 and 15.
The size of the images is of 300x300, occupying approximately 6KBs, but a preview of 100x100 can be obtained, occupying approximately 2KBs.

Parameters:
The script has several parameters:
- date: The date in format YYYY-MM-DD (for example 2006-07-02). If date isn't set, the default value is today.
- handicap: The extra numbers of the sudoku; 0 means zero extra numbers, 1 means one extra number, etc. The valid values are 0, 5, 10 and 15. If handicap isn't set, the default value is 0.
- format: The format of the file. The valid values are png, pdf or sdk (plain text). If format isn't set, the default value is png.
- thumb: Show a smaller imager for preview. This parameter only works for png format and and when solved isn't set.
- solved: Show a solved sudoku.

Examples:
To add a sudoku in a web page you only have to add the following code where you want the sudoku:
Preview with link to Python Sudoku web (recommended):
<a href=”http://pythonsudoku.sourceforge.net/index.php?view&handicap=0#sudokus”><img src=”http://pythonsudoku.sourceforge.net/sudoku.php?thumb&format=png&handicap=0” /></a>

Sudoku image with link to Python Sudoku web:
<a href=”http://pythonsudoku.sourceforge.net/index.php?view&handicap=0#sudokus”><img src=”http://pythonsudoku.sourceforge.net/sudoku.php?format=png&handicap=0” /></a>

Sudoku image with link to the solved sudoku image:
<a href=”http://pythonsudoku.sourceforge.net/sudoku.php?solved&format=png&handicap=0”><img src=”http://pythonsudoku.sourceforge.net/sudoku.php?format=png&handicap=0” /></a>
